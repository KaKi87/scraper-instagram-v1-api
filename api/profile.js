const Instagram = require('scraper-instagram');

module.exports = async fastify => {
    fastify.get('/profile/:username', async (request, reply) => {
        try {
            const client = new Instagram();
            reply.send(await client.getProfile(request.params['username']));
        }
        catch(err){
            reply.code(500).send(err.stack || err);
        }
    });
};