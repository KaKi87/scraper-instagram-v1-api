const Instagram = require('scraper-instagram');

module.exports = async fastify => {
    fastify.get('/post/:shortcode', async (request, reply) => {
        try {
            const client = new Instagram();
            reply.send(await client.getPost(
                request.params['shortcode'],
                { useGraphQL: typeof request.query['useGraphQL'] === 'string' }
            ));
        }
        catch(err){
            reply.code(500).send(err.stack || err);
        }
    });
};