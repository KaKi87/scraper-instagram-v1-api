const Instagram = require('scraper-instagram');

module.exports = async fastify => {
    let sessionId;
    fastify.register(require('fastify-auth'));
    fastify.register(require('fastify-basic-auth'), {
        authenticate: {
            realm: 'Session ID'
        },
        validate: async (undefined, _sessionId) => {
            sessionId = _sessionId;
        }
    });
    fastify.after(() => {
        fastify.route({
            method: 'GET',
            url: '/story',
            preHandler: fastify.auth([ fastify.basicAuth ]),
            handler: async (request, reply) => {
                try {
                    const { id, username } = request.query;
                    const client = new Instagram();
                    try {
                        await client.authBySessionId(sessionId);
                    }
                    catch(_){
                        return reply.code(401).send();
                    }
                    if(id)            return await client.getProfileStoryById(id);
                    else if(username) return await client.getProfileStory(username);
                    else              return await client.getAccountStories();
                }
                catch(err){
                    reply.code(500).send(err.stack || err);
                }
            }
        });
    });
};