const
    Instagram = require('scraper-instagram'),
    RSS = require('rss');

const { host } = require('../config');

module.exports = async fastify => {
    fastify.get('/rss/profile/:username/posts', async (request, reply) => {
        try {
            const { username } = request.params;
            const client = new Instagram();
            const { link, lastPosts } = await client.getProfile(username);
            const feed = new RSS({
                title: username,
                feed_url: `${host}/rss/profile/${username}/posts`,
                site_url: link
            });
            if(lastPosts && lastPosts.length){
                for(let i = 0; i < lastPosts.length; i++){
                    const { shortcode, caption } = lastPosts[i];
                    const { link, timestamp, contents } = await client.getPost(shortcode);
                    const { thumbnail, url: picture } = contents[0];
                    feed.item({
                        title: shortcode,
                        description: caption,
                        url: link,
                        date: new Date(timestamp * 1000),
                        enclosure: {
                            url: thumbnail || picture,
                            type: 'image/jpeg'
                        }
                    });
                }
            }
            reply.send(feed.xml({ indent: true }));
        }
        catch(err){
            reply.code(500).send(err.stack || err);
        }
    });
};