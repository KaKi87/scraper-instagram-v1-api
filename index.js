const fastify = require('fastify')();

const config = require('./config');

fastify.register(require('./api/profile'));

fastify.register(require('./api/story'));

fastify.register(require('./api/post'));

fastify.register(require('./api/rss'));

fastify
    .listen(process.env['PORT'] || config.port, '0.0.0.0')
    .then(() => console.log('Server running'))
    .catch(console.error);